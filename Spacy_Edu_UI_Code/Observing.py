import pickle
import glob
from sys import implementation

# with open ('101_200.pkl', "rb") as pickle_off:
#   data = pickle.load(pickle_off)
# print(len(data))
# for i in data:
#     print(i)
#     print()


# with open ('ROW_DATA.pkl', "rb") as pickle_off:
#   data1 = pickle.load(pickle_off)
#   # for i in data1:
#   #   print(i)
#   #   print()
# print(len(data1))



# import docx2txt
import spacy
import glob
import json
import docx2txt
import re
import unicodedata


# nlp1 = spacy.load(r"C:/XSeed_ML/Spacy_Edu_UI_Code/data_with star_hash/model-best") #load the best model
nlp1 = spacy.load(r"C:/XSeed_ML/Spacy_Edu_UI_Code/data_without_star_hash/model-best")

edu = [' ACADEMIC CREDENTIALS ','ACADEMIC CREDENTIALS',' PROFESSIONAL HISTORY ', ' Education and Training ', ' Education & Training ', ' Education & Training ', ' Education & Training ', ' EDUCATION AND TRAINING ',
      ' EDUCATION & TRAINING ', ' EDUCATIONAL BACKGROUND ', ' Educational Background ', ' EDUCATIONAL DETAILS ', ' Educational Details ', ' EDUCATION CERTIFICATES ',
      ' Education Certificates ', ' EDUCATIONAL CERTIFICATES ', ' Educational Certificates ', ' Education & Credentials ', ' EDUCATION & CREDENTIALS ', ' Education and Credentials ',
      ' EDUCATION AND CREDENTIALS ', ' EDUCATION & PROFESSIONAL DEVELOPMENT ', ' Education & Professional Development ', ' Education and Professional Development ',
      ' EDUCATION AND PROFESSIONAL DEVELOPMENT ', ' Academic Education & Certification ', ' Academic Education and Certification ', ' ACADEMIC EDUCATION & CERTIFICATION ',
      ' ACADEMIC EDUCATION AND CERTIFICATION ', ' Academics ', ' ACADEMICS ', ' EDUCATIONAL DOCUMENTS ', ' EDUCATION AND CERTIFICATION ', ' Education And Certification ',
      ' Education and certification ', "PROFESSIONAL HISTORY","Education and Training","Education & Training", "Education & Training", "Education & Training",
       "EDUCATION AND TRAINING","EDUCATION & TRAINING","EDUCATIONAL BACKGROUND","Educational Background",
       "EDUCATIONAL DETAILS","Educational Details", "EDUCATION CERTIFICATES", "ACADEMIC QUALIFICATIONS"," ACADEMIC QUALIFICATIONS ",
       "Education Certificates", "EDUCATIONAL CERTIFICATES","Educational Certificates","Education & Credentials",
       "EDUCATION & CREDENTIALS","Education and Credentials","EDUCATION AND CREDENTIALS","EDUCATION & PROFESSIONAL DEVELOPMENT",
       "Education & Professional Development","Education and Professional Development",
       "EDUCATION AND PROFESSIONAL DEVELOPMENT","Academic Education & Certification",
       "Academic Education and Certification","ACADEMIC EDUCATION & CERTIFICATION","ACADEMIC EDUCATION AND CERTIFICATION", 
       "Academics","ACADEMICS","EDUCATIONAL DOCUMENTS", "EDUCATION AND CERTIFICATION","Education And Certification","Education and certification",
       ' EDUCATION ', ' Education ', "EDUCATION","Education",'QUALIFICATION',' QUALIFICATION ',
       'Qualification',' Qualification ','Academic Background',' Academic Background ',' Academic Credentials ']

def date_modification(docx_file):
    docx_file=docx2txt.process(docx_file)
    docx_file=docx_file.split()
    text_data = ' '.join(docx_file)
    
    res = []
    for i in edu:
        for match in re.finditer(i, text_data):
            position=(match.start(), match.end())
            res.append(position)
    if not res:
        return (' ')
    else:
        for i in res:
            return (text_data[i[0]:i[1]+400])

files_docx=glob.glob("C:/XSeed_ML/Spacy_Edu_UI_Code/resumes/*.docx")

for i in files_docx:
    print(i)
    educations = date_modification(i)
#     print(educations)
    # print()
    doc = nlp1(educations) # input sample text
    # display in Jupyter
    # spacy.displacy.render(doc, style="ent", jupyter=True) 
    for ent in doc.ents:
        print(ent.text, "-->", ent.label_)
    print()
