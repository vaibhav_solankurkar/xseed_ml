import re
from datetime import datetime
new_dates2=[]
def date_modification(text):
    # docx_file=docx2txt.process(text)
    
    docx_file=text.split()
    docx_file = ' '.join(docx_file)
    
    date_time = datetime.today().strftime('%B %Y')
    str_date=['Current','current','Till Date','till date','Till date','till Date','- Present ','- present ','-Present','-present','Till Present',' Present']

    # string dates replaced with current dates
    for i in str_date:
        docx_file=docx_file.replace(i, date_time)

    # finding Dates with different formats
    dates1 = re.findall(r'[Jan|Feb|Mar|Apr|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December]\w+[’‘, ’ ‘][\d]{2,4}',docx_file)
    dates2 = re.findall(r"[\d]{1,2}/[\d]{2,4}",docx_file)
    dates3 = re.findall(r'[Jan|Feb|Mar|Apr|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December]\w+[’‘, ’ ‘][ ’ ‘][\d]{2,4}',docx_file)
    dates4 = re.findall(r'[Jan|Feb|Mar|Apr|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December]\w+[\d]{2,4}',docx_file)
    
    dates = dates1 + dates2 + dates3 + dates4
    rep_dates=[]
    for i in dates:
        rep_dates.append(i.replace('-',' ').replace(', ',' ').replace('/',' ').replace('.',' ').replace("'",' ').replace('’',' ').replace('‘',' ').replace(',',' ').replace('’ ','').replace(' ’','').replace(' ‘',''))

    def converting_dates(text):
        for formats in ('%b %Y','%B %Y','%b %y','%B %y','%m %y','%m %Y','%Y'):
            try:
                return datetime.strptime(text, formats).strftime('%B %Y')
            except:
                pass
        
    new_dates = []
    [new_dates.append(converting_dates(i)) for i in rep_dates]
#     print(new_dates)
#     print()    
    [new_dates2.append(i) for i in new_dates if i != None]
    
    # 'None' values replaced with old values
    mod_dates = [dates[i] if new_dates[i]== None else new_dates[i] for i in range(len(new_dates))]
#     print(mod_dates)

    # Date replaced in txt file
    for i in range(len(dates)):
        docx_file = docx_file.replace(dates[i],mod_dates[i])
    return (docx_file)

def SE_pos(text_data):
    res=[]
    for i in new_dates2:
        for match in re.finditer(i, text_data):
            date=(match.start(), match.end())
            res.append(date)
    return res
    
def delete_duplicate(res):
    return [[a, b] for i, [a, b] in enumerate(res)
    if not any(c == b for _, c in res[:i])]

def partition(arr, low, high):
	i = (low-1)		 # index of smaller element
	pivot = arr[high][1]	 # pivot

	for j in range(low, high):

		# If current element is smaller than or
		# equal to pivot
		if arr[j][1] <= pivot:

			# increment index of smaller element
			i = i+1
			arr[i], arr[j] = arr[j], arr[i]

	arr[i+1], arr[high] = arr[high], arr[i+1]
	return (i+1)

def quickSort(arr, low, high):
	if len(arr) == 1:
		return arr
	if low < high:

		# pi is partitioning index, arr[p] is now
		# at right place
		pi = partition(arr, low, high)

		# Separately sort elements before
		# partition and after partition
		quickSort(arr, low, pi-1)
		quickSort(arr, pi+1, high)

        
def combine_date(l):
    l_len = len(l)
    res=[]
    res_date=[]
    for i in range(l_len-1):
        if (l[i][1] - l[i+1][0]) > -20:
        	# res.append((l[i][0],l[i+1][1]))
        	res_date.append( (l[i],l[i+1])) 
            
    return res_date

def getDatestext(i,number_of_chars_to_date):
    # print(i)
    dates_conv = date_modification(i)
    res_data = SE_pos(dates_conv)
#     print(res_data)
    dup_removed = delete_duplicate(res_data)
    arr = dup_removed
    n = len(arr)
    quickSort(arr, 0, n-1)
    values = combine_date(arr)
    # print(values)
    data=[]
    for t in values:
      d=dict()
      # print(f"{t[0][0]} {t[1][1]}")
    	# print(dates_conv[t[0][0]:t[1][1]])
      # sample_str = sample_str[0:n] + replacement + sample_str[n+1: ]
      # dates_conv=dates_conv[0:t[0][0]]+f"{dates_conv[t[0][0]:t[0][1]]} -- {dates_conv[t[1][0]:t[1][1]]}"+dates_conv[t[1][1]+1:]
      # print(dates_conv[t[0][0]:t[1][1]])
      # print(f"{dates_conv[t[0][0]:t[0][1]]} --------{dates_conv[t[1][0]:t[1][1]]}")
      d['period']=f"{dates_conv[t[0][0]:t[0][1]]} -- {dates_conv[t[1][0]:t[1][1]]}"
      d['text']=dates_conv[t[0][0]-number_of_chars_to_date:t[1][1]+number_of_chars_to_date]
      d['text']=d['text'].replace(dates_conv[t[0][0]:t[1][1]],f"{dates_conv[t[0][0]:t[0][1]]} -- {dates_conv[t[1][0]:t[1][1]]}")
      # tt=dates_conv[t[0][0]-number_of_chars_to_date:t[1][1]+number_of_chars_to_date]
      # print(f"{t[0][0]} {t[1][1]}")
      # print((dates_conv[t[0][0]:t[1][1]]))
      # tt=tt.replace(dates_conv[t[0][0]:t[1][1]],f"{dates_conv[t[0][0]:t[0][1]]} -- {dates_conv[t[1][0]:t[1][1]]}")
      # print(tt)
      data.append(d)
    return data
if __name__ == '__main__':
  filename='/content/drive/My Drive/ResumeParser/Flair/Resumes_Kaggle/Adelina_Erimia_PMP1.docx'
  txt = docx2txt.process(filename)
  txt=txt.replace("\n"," # ")
  txt=txt.replace("\t"," * ")
  dt=getDatestext(txt,200)
  for d in dt:
      sentence = Sentence(d['text']) # input sample text
      print(d['period'])
      print(sentence.to_tokenized_string())
