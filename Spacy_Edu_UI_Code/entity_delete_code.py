import pickle
import glob

data = []
all_files = glob.glob("C:/XSeed_ML/Spacy_Edu_UI_Code/pickle_files_without_hash/*.pkl")
for i in all_files:
  print(i)
  with open (i, "rb") as pickle_off:
    data1 = pickle.load(pickle_off)
    data += data1

for i in data:
  dic_list = []
  for x, y in i[1].items():
    for j in y:
      if j[2] == 'DATE':
        del j
      else:
        dic_list.append(j)
    y.clear()
    y += dic_list

with open("C:/XSeed_ML/Spacy_Edu_UI_Code/Data_WithOut_Star_Hash.pkl", 'wb') as fh:
   pickle.dump(data, fh)

# with open ('C:/XSeed_ML/Spacy_Edu_UI_Code/Data_Without_Star_Hash.pkl', "rb") as pickle_off:
#   data = pickle.load(pickle_off)
# for i in data:
#     print(i)



