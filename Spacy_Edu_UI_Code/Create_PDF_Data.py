# !export PYTHONPATH="${PYTHONPATH}:/home/sam/Desktop/exp/"
import docx2txt
from yourapp.filetotxt import filetoText
import yourapp.extractDateText as extractDateText
import glob 
import re
import fitz



def extract_education_data(docx_file):
    edu = [' ACADEMIC CREDENTIALS ','ACADEMIC CREDENTIALS',' PROFESSIONAL HISTORY ', ' Education and Training ', ' Education & Training ', ' Education & Training ', ' Education & Training ', ' EDUCATION AND TRAINING ',
      ' EDUCATION & TRAINING ', ' EDUCATIONAL BACKGROUND ', ' Educational Background ', ' EDUCATIONAL DETAILS ', ' Educational Details ', ' EDUCATION CERTIFICATES ',
      ' Education Certificates ', ' EDUCATIONAL CERTIFICATES ', ' Educational Certificates ', ' Education & Credentials ', ' EDUCATION & CREDENTIALS ', ' Education and Credentials ',
      ' EDUCATION AND CREDENTIALS ', ' EDUCATION & PROFESSIONAL DEVELOPMENT ', ' Education & Professional Development ', ' Education and Professional Development ',
      ' EDUCATION AND PROFESSIONAL DEVELOPMENT ', ' Academic Education & Certification ', ' Academic Education and Certification ', ' ACADEMIC EDUCATION & CERTIFICATION ',
      ' ACADEMIC EDUCATION AND CERTIFICATION ', ' Academics ', ' ACADEMICS ', ' EDUCATIONAL DOCUMENTS ', ' EDUCATION AND CERTIFICATION ', ' Education And Certification ',
      ' Education and certification ', "PROFESSIONAL HISTORY","Education and Training","Education & Training", "Education & Training", "Education & Training",
       "EDUCATION AND TRAINING","EDUCATION & TRAINING","EDUCATIONAL BACKGROUND","Educational Background",
       "EDUCATIONAL DETAILS","Educational Details", "EDUCATION CERTIFICATES", "ACADEMIC QUALIFICATIONS"," ACADEMIC QUALIFICATIONS ",
       "Education Certificates", "EDUCATIONAL CERTIFICATES","Educational Certificates","Education & Credentials",
       "EDUCATION & CREDENTIALS","Education and Credentials","EDUCATION AND CREDENTIALS","EDUCATION & PROFESSIONAL DEVELOPMENT",
       "Education & Professional Development","Education and Professional Development",
       "EDUCATION AND PROFESSIONAL DEVELOPMENT","Academic Education & Certification",
       "Academic Education and Certification","ACADEMIC EDUCATION & CERTIFICATION","ACADEMIC EDUCATION AND CERTIFICATION", 
       "Academics","ACADEMICS","EDUCATIONAL DOCUMENTS", "EDUCATION AND CERTIFICATION","Education And Certification","Education and certification",
       ' EDUCATION ', ' Education ', "EDUCATION","Education"]
    
    docx_file=docx_file.split()
    text_data = ' '.join(docx_file)
    
    res = []
    for i in edu:
        for match in re.finditer(i, text_data):
            position=(match.start(), match.end())
            res.append(position)
    if not res:
        return None
    else:
        print(len(res))
        print((res))
        for i in res:
            return (text_data[i[0]:i[1]+500])

docx_files=(glob.glob("F:/tCognition/XSeed Machine Learning/All Resume/pdf files/*.pdf"))

data=[]
for filename in docx_files:
  try:
        with fitz.open(filename) as doc:
            text = ""
            for page in doc:
                text = text + str(page.getText())
            txt = ''.join(text)    
  except Exception as inst:
        print("\nError in reading  pdf from bytes")
        # print(inst)
        #print(len(b))
        #print(base64EncodedData.endswith("="))
        # print("\n")
  if(txt):
    txt = txt.split()
    txt = ' '.join(txt)
    # txt=txt.replace("\n"," # ")
    # txt=txt.replace("\t"," * ")
    dt=extract_education_data(txt)
    
    if(dt):
      print(f"\n\n{filename}")
      print(dt)
      data.append(dt)

import pickle
with open('Raw_Data.pkl', 'wb') as fh:
   pickle.dump(data, fh)
