import tkinter as tk

import pickle
with open ('ROW_DATA.pkl', "rb") as pickle_off:
  data = pickle.load(pickle_off)
# print(f"Total number of records: {len(data)}")
records_file_name='801_850.pkl'


txt=None
records=[]
TEXT=None
ENTITY_LIST=[]
ENTITY=None
ed_pos = [0,0]

def isBlank (myString):
    if myString and myString.strip():
        #myString is not None AND myString is not empty or blank
        return False
    #myString is None OR myString is empty or blank
    return True
def copy_text(event):
    try:
        global txt
        txt = readOnlyText.get('sel.first', 'sel.last')
    except tk.TclError:
        # print("# no selection")        
        return "break"
    except Exception as e:
        # print("Error in reading")
        return "break"
    # print(f"Selected text: {txt}")
    #got selected text in txt
    if(isBlank(txt)):
        # print("Blank selection")
        return "break"
    return "break"  # prevent class binding to be triggered

def create_record_company(event):
    # print(f"Record created {txt} {event} {event.__dict__}")
    global ENTITY_LIST
    if(isBlank(txt)):
        return "break"

    len_txt = len(txt)
    stpos = TEXT[ed_pos[1]:].find(txt) + ed_pos[1]
    edpos = stpos + len_txt
    ed_pos.pop()
    ed_pos.append(edpos)
    ENTITY = (stpos,edpos, 'COMPANY')
    ENTITY_LIST.append(ENTITY)
    return "break"

def create_record_location(event):
    # print(f"Record created {txt} {event} {event.__dict__}")
    global ENTITY_LIST

    if(isBlank(txt)):
        return "break"

    len_txt = len(txt)
    stpos = TEXT[ed_pos[1]:].find(txt) + ed_pos[1]
    edpos = stpos + len_txt
    ed_pos.pop()
    ed_pos.append(edpos)
    ENTITY = (stpos,edpos, 'LOCATION')
    ENTITY_LIST.append(ENTITY)
    return "break"

def create_record_role(event):
    # print(f"Record created {txt} {event} {event.__dict__}")
    global ENTITY_LIST

    if(isBlank(txt)):
        return "break"

    len_txt = len(txt)
    stpos = TEXT.find(txt)
    edpos = stpos + len_txt
    ENTITY = (stpos,edpos, 'ROLE')
    ENTITY_LIST.append(ENTITY)
    return "break"

root = tk.Tk()
readOnlyText = tk.Text(root, fg="dark green")
readOnlyText.configure(state='normal',font=("Courier", 16, "italic"))
readOnlyText.place(x=0,y=0,width=1000) 
readOnlyText.bind('<Control-c>', copy_text)
readOnlyText.delete('1.0', tk.END)
readOnlyText.insert('end',data[0]['text'])
TEXT=data[0]['text']

counter = 0
def getNextData():
    global counter
    global TEXT
    global ENTITY_LIST
    global records

    date = data[counter]['period']
    len_txt = len(date)
    stpos = TEXT.find(date)
    edpos = stpos + len_txt
    ENTITY = (stpos,edpos, 'DATE')
    # print(TEXT)
    # print('xwegnfciwamuhf9uwfeojlkduw3;offed   ',ENTITY)
    ENTITY_LIST.append(ENTITY)
    records.append((TEXT,{'entities':ENTITY_LIST}))
    ENTITY_LIST=[]
    # for r in records:
    #     print(r)
    import pickle
    with open(records_file_name, 'wb') as fh:
        pickle.dump(records, fh)
    counter += 1
    if(counter>len(data)):
        return 'break'
    readOnlyText.delete('1.0', tk.END)
    try:
        readOnlyText.insert('end',data[counter]['text'])
        TEXT=data[counter]['text']        
    except IndexError:
        pass 

    ed_pos.pop()
    ed_pos.append(0)  
    
def quit(event):
    with open(records_file_name, 'wb') as fh:
        pickle.dump(records, fh)
    root.destroy()

btn_COMPANY = tk.Button(root, text = 'COMPANY')
btn_COMPANY.place(x=1100, y=0)
btn_COMPANY.bind('<Button-1>', create_record_company)

btn_ROLE = tk.Button(root, text = 'ROLE')
btn_ROLE.place(x=1100, y=40)
btn_ROLE.bind('<Button-1>', create_record_role)

btn_LOCATION = tk.Button(root, text = 'LOCATION')
btn_LOCATION.place(x=1100, y=80)
btn_LOCATION.bind('<Button-1>', create_record_location)

btn_NEXT = tk.Button(root, text = 'NEXT',command = getNextData)
btn_NEXT.place(x=1100, y=220)

btn_q = tk.Button(root, text = 'Quit!')
btn_q.place(x=1100, y=460)
btn_q.bind('<Button-1>', quit)
root.mainloop()
