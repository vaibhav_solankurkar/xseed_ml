# !export PYTHONPATH="${PYTHONPATH}:/home/sam/Desktop/exp/"
from logging import exception
import docx2txt
from yourapp.filetotxt import filetoText
import yourapp.extractDateText as extractDateText
# from flair.data import Sentence
import glob 
docx_files=(glob.glob("C:/Users/Shital Chougule/Desktop/dates of resumes/Jason Sadler.docx"))
# /content/drive/My Drive/ResumeParser/Flair/Resumes_Kaggle/Adelina_Erimia_PMP1.docx
data=[]
for filename in docx_files:
  try:
    txt = docx2txt.process(filename)
  except:
    print("file is not supported")
    continue
  if(txt):
    # print(f"\n\n\n{filename}")
    txt=txt.replace("\n"," # ")
    txt=txt.replace("\t"," * ")
    dt=extractDateText.getDatestext(txt,150)
    print(dt)
    for d in dt:

      date = d['period'] 
      d['text'] = ' '.join(d['text'].split()[1:-1])
      # print(d)
      data.append(d)


import pickle
with open('ROW_DATA.pkl', 'wb') as fh:
   pickle.dump(data, fh)
