import re
import datetime as dt
from datetime import datetime


new_dates2=[]
def date_modification(text):
    # docx_file=docx2txt.process(text)
    docx_file=text.split()
    docx_file = ' '.join(docx_file)
    date_time = datetime.today().strftime(' %B %Y ')
    str_date=[' present ','Current','current','Till Date','till date','Till date','till Date','- Present ','- present ','-Present','-present','Till Present',' Present']
    
    # string dates replaced with current dates
    for i in str_date:
        docx_file=docx_file.replace(i, date_time)
    print("docx_file :",docx_file)
    # finding Dates with different formats
    dates1 = re.findall(r'[Jan|Feb|Mar|Apr|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December]\w+[’‘, ’ ‘][\d]{2,4}',docx_file)
    dates2 = re.findall(r"[\d]{1,2}/[\d]{2,4}",docx_file)
    dates3 = re.findall(r'[Jan|Feb|Mar|Apr|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December]\w+[’‘, ’ ‘][ ’ ‘][\d]{2,4}',docx_file)
    dates4 = re.findall(r'[Jan|Feb|Mar|Apr|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December]\w+[\d]{2,4}',docx_file)
    dates5 = re.findall(r"[\d]{4}-[\d]{4}",docx_file)
    dates7 = re.findall(r"[\d]{4} - [\d]{4}",docx_file)
    dates8 = re.findall(r"[\d]{2,4}[-/'~`=\][’‘, ’ ‘][\d]{2,4}",docx_file)
    print('dates7 :',dates7)
    print('dates8 :',dates8)
    only_dates = []
    for val in dates5:
        only_dates.append((val[:4]))
        only_dates.append((val[5:]))
        # print(only_date)

    for val in dates7:
        only_dates.append((val[:4]))
        only_dates.append((val[7:]))
        # print(only_dates)    
    import random
    dates6 = []
    now = dt.datetime.now().year
    for i in only_dates:
        j = i.strip()
        if 1990 <= int(j) and int(j) <= now:
            # print(j)
            raw_month = ["January", "February", "March", "April", "May", "June"]
            random_month = random.choice(raw_month)
            m_data = f"{random_month} {j}"
            dates6.append(str(m_data))
            # print(dates6)
    dates = dates1 + dates2 + dates3 + dates4 + dates6
    # print('dates :',len(dates))

    rep_dates = []
    for j in dates:
        i = j.strip()
        rep_dates.append(i.replace(' - ',' ').replace('-',' ').replace(', ',' ').replace('/',' ').replace('.',' ').replace("'",' ').replace('’',' ').replace('‘',' ').replace(',',' ').replace('’ ','').replace(' ’','').replace(' ‘',''))

    def converting_dates(text):
        for formats in ('%b %Y','%B %Y','%b %y','%B %y','%m %y','%m %Y','%Y'):
            try:
                return datetime.strptime(text, formats).strftime('%B %Y')
            except:
                pass
        
    new_dates = []
    [new_dates.append(converting_dates(i)) for i in rep_dates]
    # print(new_dates)
#     print()    
    [new_dates2.append(i) for i in new_dates if i != None]
    # print(new_dates2)
    # 'None' values replaced with old values
    mod_dates = [dates[i] if new_dates[i]== None else new_dates[i] for i in range(len(new_dates))]
    # print(mod_dates)
    # Date replaced in txt file
    for i in range(len(dates)):
        docx_file = docx_file.replace(dates[i],mod_dates[i])
    return (docx_file)

def SE_pos(text_data):
    res=[]
    for i in new_dates2:
        for match in re.finditer(i, text_data):
            date=(match.start(), match.end())
            res.append(date)
    return res
    
def delete_duplicate(res):
    return [[a, b] for i, [a, b] in enumerate(res)
    if not any(c == b for _, c in res[:i])]

def partition(arr, low, high):
	i = (low-1)		 # index of smaller element
	pivot = arr[high][1]	 # pivot
	for j in range(low, high):
		# If current element is smaller than or
		# equal to pivot
		if arr[j][1] <= pivot:
			# increment index of smaller element
			i = i+1
			arr[i], arr[j] = arr[j], arr[i]
	arr[i+1], arr[high] = arr[high], arr[i+1]
	return (i+1)

def quickSort(arr, low, high):
	if len(arr) == 1:
		return arr
	if low < high:
		# pi is partitioning index, arr[p] is now
		# at right place
		pi = partition(arr, low, high)
		# Separately sort elements before
		# partition and after partition
		quickSort(arr, low, pi-1)
		quickSort(arr, pi+1, high)
   
def combine_date(l):
    l_len = len(l)
    res_date=[]
    for i in range(l_len-1):
        if (l[i][1] - l[i+1][0]) > -20:
        	res_date.append( (l[i],l[i+1]))
    return res_date

def getDatestext(i,number_of_chars_to_date):
    # print(i)
    dates_conv = date_modification(i)
    # print('dates_conv',dates_conv)
    res_data = SE_pos(dates_conv)
#     print(res_data)
    dup_removed = delete_duplicate(res_data)
    arr = dup_removed
    n = len(arr)
    quickSort(arr, 0, n-1)
    values = combine_date(arr)
    # print(values)
    data=[]
    for t in values:
      d=dict()
      d['period']=f"{dates_conv[t[0][0]:t[0][1]]} -- {dates_conv[t[1][0]:t[1][1]]}"
      d['text']=dates_conv[t[0][0]-number_of_chars_to_date:t[1][1]+number_of_chars_to_date]
      d['text']=d['text'].replace(dates_conv[t[0][0]:t[1][1]],f"{dates_conv[t[0][0]:t[0][1]]} -- {dates_conv[t[1][0]:t[1][1]]}")
      data.append(d)
    return data
if __name__ == '__main__':
#   filename = "C:/Users/Shital Chougule/Desktop/UI_resumes/Detailed IT PM Manager 11282016.docx"
  filename = r"C:\Users\Shital Chougule\Desktop\dates of resumes\dates of resumes.docx"
  import docx2txt
  txt = docx2txt.process(filename)
  txt=txt.replace("\n"," # ")
  txt=txt.replace("\t"," * ")
  dt=getDatestext(txt,100)
#   for d in dt:
#       print(d)
#       sentence = Sentence(d['text']) # input sample text
#       print(d['period'])
#       print(sentence.to_tokenized_string())
