# coding=utf-8
import fitz
import docx2txt
# from  mydocx2txt import mydocx2txt
from  yourapp.mydocx2txt import mydocx2txt
import string
from yourapp.striprtf import rtf_to_text
 

def filetoText(file_path,docType):
    if(docType==".pdf"):
        txt=pdfToText(file_path)
        return txt
    if(docType==".docx"):
        txt=docxToText(file_path)
        return txt
    if(docType==".rtf"):
        txt=rtfToText(file_path)
        return txt

def rtfToText(file_path):
    txt=None
    
    try:
        txt=rtf_to_text(open(file_path).read())
        # print("in rtfTOtext")
        print(type(txt))
         
    except Exception as inst:
        print("\nError in reading  rtf from bytes")
        print(inst)
        #print(len(b))
        #print(base64EncodedData.endswith("="))
        print("\n")
    # print(txt)
    return txt

def docxToText(file_path):
    txt=None
   
    try:
        txt=mydocx2txt(file_path)
    except Exception as inst:
        print("\nError in mydocx2txt")
        print(inst)
        print("\n")
    
    # print(f"{type(txt)} {repr(txt)}")
    return txt

def pdfToText(file_path):
    txt=None
    
    try:
        with fitz.open(file_path) as doc:
            text = ""
            for page in doc:
                text = text + str(page.getText())
            txt = ''.join(x for x in text if x in string.printable)    
    except Exception as inst:
        print("\nError in reading  pdf from bytes")
        print(inst)
        #print(len(b))
        #print(base64EncodedData.endswith("="))
        print("\n")
    # print(txt)
    return txt


if __name__ == "__main__":
    # path_pdf="C:/Users/sandip more/Desktop/ResumeParsing/flair/Resumes_Kaggle/Abiral_Pandey_Fullstack_Java.pdf"
    # text=filetoText(path_pdf,".pdf")
    # print(text)
    
    # path_docx="C:/Users/sandip more/Desktop/ResumeParsing/flair/Resumes_Kaggle/Achyuth Resume_8.docx"
    # text=filetoText(path_docx,".docx")
    # print(text)

    path_docx="C:/Users/sandip more/Downloads/rtfdocs/Allen_Alloway - BI Lead.rtf"
    text=filetoText(path_docx,".rtf")
    print(text)
    

