
from yourapp.filetotxt import filetoText
import re
import yourapp.extractDateText as extractDateText
from yourapp.skills import extractSkills, getEmailPhonefromText,getNameSummaryfromText
import os
import json
from pathlib import Path
import spacy
import glob
import json
import docx2txt
import re
import unicodedata 

from flask import Flask, render_template, request, redirect, url_for, abort,jsonify




model_uri_experience =r"./Models/model_experience.spacy"
nlp_experience = spacy.load(model_uri_experience) 

model_uri_education =r"./Models/model-best-Education"
nlp_education = spacy.load(model_uri_education) 


app=Flask(__name__,template_folder='templates')
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.docx', '.pdf','.rtf']
app.config['UPLOAD_PATH'] = 'uploads'

edu = [' ACADEMIC CREDENTIALS ','ACADEMIC CREDENTIALS',' PROFESSIONAL HISTORY ', ' Education and Training ', ' Education & Training ', ' Education & Training ', ' Education & Training ', ' EDUCATION AND TRAINING ',
      ' EDUCATION & TRAINING ', ' EDUCATIONAL BACKGROUND ', ' Educational Background ', ' EDUCATIONAL DETAILS ', ' Educational Details ', ' EDUCATION CERTIFICATES ',
      ' Education Certificates ', ' EDUCATIONAL CERTIFICATES ', ' Educational Certificates ', ' Education & Credentials ', ' EDUCATION & CREDENTIALS ', ' Education and Credentials ',
      ' EDUCATION AND CREDENTIALS ', ' EDUCATION & PROFESSIONAL DEVELOPMENT ', ' Education & Professional Development ', ' Education and Professional Development ',
      ' EDUCATION AND PROFESSIONAL DEVELOPMENT ', ' Academic Education & Certification ', ' Academic Education and Certification ', ' ACADEMIC EDUCATION & CERTIFICATION ',
      ' ACADEMIC EDUCATION AND CERTIFICATION ', ' Academics ', ' ACADEMICS ', ' EDUCATIONAL DOCUMENTS ', ' EDUCATION AND CERTIFICATION ', ' Education And Certification ',
      ' Education and certification ', "PROFESSIONAL HISTORY","Education and Training","Education & Training", "Education & Training", "Education & Training",
       "EDUCATION AND TRAINING","EDUCATION & TRAINING","EDUCATIONAL BACKGROUND","Educational Background",
       "EDUCATIONAL DETAILS","Educational Details", "EDUCATION CERTIFICATES", "ACADEMIC QUALIFICATIONS"," ACADEMIC QUALIFICATIONS ",
       "Education Certificates", "EDUCATIONAL CERTIFICATES","Educational Certificates","Education & Credentials",
       "EDUCATION & CREDENTIALS","Education and Credentials","EDUCATION AND CREDENTIALS","EDUCATION & PROFESSIONAL DEVELOPMENT",
       "Education & Professional Development","Education and Professional Development",
       "EDUCATION AND PROFESSIONAL DEVELOPMENT","Academic Education & Certification",
       "Academic Education and Certification","ACADEMIC EDUCATION & CERTIFICATION","ACADEMIC EDUCATION AND CERTIFICATION", 
       "Academics","ACADEMICS","EDUCATIONAL DOCUMENTS", "EDUCATION AND CERTIFICATION","Education And Certification","Education and certification",
       ' EDUCATION ', ' Education ', "EDUCATION","Education"]

def extract_education_data(docx_file):
    # try:
    #     docx_file=docx2txt.process(docx_file)
    # except Exception as e:
    #     print(e,docx_file)
    #     return " "
    docx_file=docx_file.split()
    text_data = ' '.join(docx_file)
    
    res = []
    for i in edu:
        for match in re.finditer(i, text_data):
            position=(match.start(), match.end())
            res.append(position)
    if not res:
        return (' ')
    else:
        print(len(res))
        print((res))
        for i in res:
            return (text_data[i[0]:i[1]+400])

dfa = {0:{'UNIVERSITY':2, 'QUALIFICATION':3,'EOL':1},
      #  1:{'UNIVERSITY':0, 'QUALIFICATION':1,'EOL':2},
       2:{'UNIVERSITY':4, 'QUALIFICATION':0,'EOL':1},
       3:{'UNIVERSITY':0, 'QUALIFICATION':4,'EOL':1},
       4:{'UNIVERSITY':4, 'QUALIFICATION':4,'EOL':4}
       }

def accepts(transitions,initial,accepting,s):
    # {"School":"School","Course":"Course","StartDate":"StartDate","EndDate":"EndDate"}
    result=[]
    L=[]
    state = initial
    flag=False
    for c in s:
        # print(c[0],state)
        if(state in [4]):
            flag=True
            break
        if(state==0 and c[0] != 'EOL'):
            L=[]
            L.append(c)
        if(state in [2,3] and c[0] != 'EOL'):
            L.append(c)
            result.append(L)

        state = transitions[state][c[0]]
    if(flag):
        return None
    else:
        return result
        # return     return state in accepting

def getbase64encodedString(txt):
    match=re.match(r"data:application/.+;base64,",txt)
    if(match):
        return txt[match.end():]
    return u""


def getResposeDataFields(file):
    p = Path(__file__).with_name(file)
    with p.open('r') as f:

    # with open(file,"r") as f:
        data = json.load(f)
        return data 
def getskillsList(x:dict)->list:
    l=list(x.values())
    L=[]
    for i in l:
        if(isinstance(i,list)):
            for j in i:
                L.append(j)
        else:
            L.append(i)
    return L



@app.route('/', methods=['POST'])
def upload_files():

    #(candidateid,filepath)
    data=request.get_data()

    # start = time.process_time()
    rec_data=json.loads(data)

    


    
    
    # filename = secure_filename(rec_data["name"])
    filename = rec_data["file_path"]
    print(filename)
    if filename != '':
        file_ext = os.path.splitext(filename)[1]
        # print(file_ext)
       
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            return jsonify(dict({"error":"Unknown file extension"}))
        
   
        print(f"{filename}   {file_ext}")
        txt=filetoText(filename,file_ext)
        
        print("in flask")
        print(txt)

        educations = extract_education_data(txt)
        print(educations)
        doc_edu = nlp_education(educations) # input sample t
        Edu_Ent=[(ent.label_,ent.text) for ent in doc_edu.ents]
        Edu_Ent.append(("EOL","EOL"))
        for lbl,t in Edu_Ent:
           print(lbl,t)
        res=accepts(dfa,0,{1},Edu_Ent)
        print('res:',res)
        edu_d_l=[]
        if(res):
            for i in res:
                if(len(i)==2):
                    if(i[0][0]=='QUALIFICATION' and i[1][0]=='UNIVERSITY'):
                        dd={"School":i[1][1],"Course":i[0][1],"StartDate":"StartDate","EndDate":"EndDate"}
                        edu_d_l.append(dd)
                   # print(dd)
                    if(i[1][0]=='QUALIFICATION' and i[0][0]=='UNIVERSITY'):
                        dd={"School":i[0][1],"Course":i[1][1],"StartDate":"StartDate","EndDate":"EndDate"}
                   # print(dd)        
                        edu_d_l.append(dd)
        


        #extract employment data  
        """                /* Employement Info */

                model.Positions = new List<Position>();
                foreach (var position in parsedResume.Positions)
                {
                    Position positionModel = new Position();
                    positionModel.Company = position.Company;
                    positionModel.Summary = position.Summary;
                    positionModel.StartDate = position.StartDate;
                    positionModel.EndDate = position.EndDate;
                    model.Positions.Add(positionModel);
                }
"""
        dt=extractDateText.getDatestext(txt)
        ExtractedDataExpereince=[]
        for d in dt:
            print(f"date: {d} {d['period'].split('--')}")
            doc = nlp_experience(d['text'])
            print(type(doc))
            dd=dict()
            
            
            company_list=[ent.text for ent in doc.ents if(ent.label_=="COMPANY")]
            print(f"company names:----  {company_list}")
            role_list=[ent.text for ent in doc.ents if(ent.label_=="ROLE")]
            print(f"roles :----  {role_list}")

            dd["Company"]=None
            if(len(company_list)>0):
                dd["Company"]=company_list[0]

            dd["Summary"]=None#add job role for time being
            if(len(role_list)>0):
                dd["Summary"]=[(role_list[0])]
    
            psl=d['period'].split('--')
            if(len(psl)==0):
                dd["StartDate"]=None
                dd["EndDate"]=None
            if(len(psl)==1):
                dd["StartDate"]=psl[0]
                dd["EndDate"]=None
            if(len(psl)==2):
                dd["StartDate"]=psl[0]
                dd["EndDate"]=psl[1]
                

            ExtractedDataExpereince.append(dd)
        
        #extract skills
        skills_dict=extractSkills(txt)

        print(f"{repr(skills_dict)} {type(skills_dict)}")
        Skills=None
        if(skills_dict):
            print("Skills found")
            Skills=getskillsList(skills_dict)
     


        #create response
        ParsedResume=getResposeDataFields('output.json')
        ParsedResume["Skills"]=Skills 

        
        EmailPhone=getEmailPhonefromText(txt)
        """
        EmailAddress
        Phone
        Mobile
        Location

        """ 
        """public List<Education> Educations { get; set; }
            --------------Education--------------------------
            public string School { get; set; }
            public string Course { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }"""
        ParsedResume['Educations']=edu_d_l
        print(edu_d_l)
        ParsedResume["EmailAddress"]=None
        if(len(EmailPhone["Email"])>0):
            ParsedResume["EmailAddress"]=EmailPhone["Email"][0]

        ParsedResume["Phone"]=None
        if(len(EmailPhone["PhoneNumber"])>0):
            ParsedResume["Phone"]=EmailPhone["PhoneNumber"][0]

        ParsedResume["Mobile"]=None
        if(len(EmailPhone["PhoneNumber"])>0):
            ParsedResume["Mobile"]=EmailPhone["PhoneNumber"][0]


        ParsedResume["FirstName"]=None #need to be changed
        ParsedResume["LastName"]=None #need to be changed
        ParsedResume["MiddleName"]=None #need to be changed
        # ParsedResume["Mobile"]=None #need to be changed
        ParsedResume["PrimaryEmail"]=ParsedResume["EmailAddress"] #need to be changed
        ParsedResume["ProfileSummary"]=None

        name=getNameSummaryfromText(txt)
        if(name):
            lst=name.split()
            if(len(lst)>3):
                pass
            if(len(lst)==3):
               ParsedResume["FirstName"]=lst[0] #need to be changed
               ParsedResume["LastName"]=lst[2] #need to be changed
               ParsedResume["MiddleName"]=lst[1] #need to be changed
            if(len(lst)==2):
               ParsedResume["FirstName"]=lst[0] #need to be changed
               ParsedResume["LastName"]=lst[1] #need to be changed
            #    ParsedResume["MiddleName"]=lst[2] #need to be changed
            if(len(lst)==1):
               ParsedResume["FirstName"]=lst[0] #need to be changed
            #    ParsedResume["LastName"]=lst[1] #need to be changed
            #    ParsedResume["MiddleName"]=lst[2] #need to be changed

        ParsedResume["EmployementHistory"]=ExtractedDataExpereince
        ParsedResume["Positions"]=ExtractedDataExpereince
        json_reponse=dict()
        json_reponse["Status"]="200"
        json_reponse["Data"]={"ParsedResume":ParsedResume}
        json_reponse["Message"]="Success"


    # # return jsonify(ExtractedDataExpereince)
    return jsonify(json_reponse)

    # return jsonify(dict({"filename":filename}))
    # return redirect(url_for('index'))



@app.route('/')
def hello_world():
    return 'Hello Flask under Apache in docker!'

if __name__ == "__main__":
    app.run(debug=False)





 

